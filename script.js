require("dotenv").config()
const MongoClient = require('mongodb/lib/mongo_client');
const ObjectID = require('mongodb').ObjectID;

const client = new MongoClient(process.env.DB_URL);
let db = null;

const list = [
    {
        name: 'Bratmann',
        url: 'https://cdnb.artstation.com/p/assets/images/images/009/986/779/large/felipe-araujo-img-20180321-113547-131.jpg?1521947750'
    },
    {
        name: 'Nuls',
        url: 'https://img.discogs.com/aaBdFC1GC-nJD_44Pas_BMm6r64=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/A-1695084-1268931804.jpeg.jpg'
    },
    {
        name: 'nodejs',
        url: 'https://www.premiere.fr/sites/default/files/styles/scale_crop_1280x720/public/2018-04/diehardtv.jpg'
    },
    {
        name: 'test',
        url: 'https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/befbcde0-9b36-11e6-95b9-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg'
    }, {
        name: 'toto',
        url: 'https://resources.world.rugby/worldrugby/photo/2017/09/20/6b6b2b32-b1d2-4725-9327-49603b501e54/Toto-logo.jpg'
    },
    {
        name: 'toucan',
        url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Ramphastos_toco_1.jpg/290px-Ramphastos_toco_1.jpg'
    },
    {
        name: 'Bratmann',
        url: 'https://cdnb.artstation.com/p/assets/images/images/009/986/779/large/felipe-araujo-img-20180321-113547-131.jpg?1521947750'
    },
    {
        name: 'Nuls',
        url: 'https://img.discogs.com/aaBdFC1GC-nJD_44Pas_BMm6r64=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/A-1695084-1268931804.jpeg.jpg'
    }
];
client.connect(function (err) {
    if (err) throw err;
    db = client.db(process.env.DB_NAME);
    list.forEach(element => {
        db.collection('pictures').insert(element, (err, res) => {
            if (err) throw err;
            if (res) {
                console.log('ajout d\'une ligne')
            }
        });
    });
});
