const mongodb = require('mongodb');
const Schema = mongodb.Schema;

let PictureSchema = new Schema({
    name: {type: String, required: true, max: 100},
    url: {type: String, required: true, max: 255 },
});

module.exports = mongodb.model('Picture', PictureSchema);