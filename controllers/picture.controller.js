// const Picture = require('../models/picture.model');
const MongoClient = require('mongodb/lib/mongo_client');
const ObjectID = require('mongodb').ObjectID;

const client = new MongoClient(process.env.DB_URL);
let db = null;
client.connect(function (err) {
    if (err) throw err;
    db = client.db(process.env.DB_NAME);
});

// CREATE
function picture_create(req, response) {
    let picture =
        {
            name: req.body.name,
            url: req.body.url
        };

    db.collection('pictures').findOne({name: req.body.name}, (err, res) => {
        if (err) throw err;
        if (res) {
            response.send('creation échouée');
        } else {
            db.collection('pictures').insert(picture, (err, res) => {
                if (err) throw err;
                if (res) {
                    response.redirect('/pictures');
                }
            });
        }
    })
}

// INDEX
function get_pictures() {
    return new Promise((callback) => {
        db.collection('pictures').find({}).toArray((err, result) => {
            if (err){
                console.log('DB ERROR !')
            }else{
                callback(result);
            }
        });
    })

}

// UPDATE
function picture_update(req, response) {
    db.collection('pictures').findOne({_id: new ObjectID(req.body.idPicture)}, function (err, result) {
            if (err) throw err;
            if (result) {
                db.collection('pictures').updateOne({_id: new ObjectID(req.body.idPicture)}, {
                    $set: {
                        name: req.body.name,
                        url: req.body.url
                    }
                }, function (err, res) {
                    if (err) throw err;
                    response.redirect('/pictures');
                });
            } else {
                response.send('update échoué')
            }
        }
    );
}

// DELETE
function picture_delete(req, response) {
    db.collection('pictures').findOne({_id: new ObjectID(req.body.idPicture)}, function (err, result) {
            if (err) throw err;
            if (result) {
                db.collection('pictures').deleteOne({_id: new ObjectID(req.body.idPicture)}, function (err, result) {
                    if (err) throw err;
                    response.redirect('/pictures');
                });
            } else {
                response.send('delete echoué')
            }
        }
    );
}

module.exports = {picture_create, picture_update, picture_delete, get_pictures};