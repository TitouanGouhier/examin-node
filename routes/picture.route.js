var express = require('express');
var router = express.Router();
var picture_controller = require('../controllers/picture.controller');
const { route } = require('.');

/* POST picture CREATE */
router.post('/create', picture_controller.picture_create);

/* PUT pictures update. */
router.post('/update', picture_controller.picture_update);

/* DELETE pictures delete. */
router.post('/delete', picture_controller.picture_delete);

/* GET pictures list */
router.get('/', function (req, res, next) {
    picture_controller.get_pictures().then(function (resultat) {
        res.render('pictures', {pictures: resultat});
    });
});

module.exports = router;
