var express = require('express');
const picture = require("../controllers/picture.controller");
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    picture.get_pictures().then(function (resultat) {
        console.log(resultat);
        res.render('index', {pictures: resultat})
    })
});

module.exports = router;
