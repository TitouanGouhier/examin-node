const express = require('express');
require("dotenv").config()
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');
const picture = require('./routes/picture.route');
const index = require('./routes/index');
let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.set('appName', 'BonjourCA');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/pictures', picture)
app.use('/', index)

// app.all('*', function (req, res) {
//   res.render('index', {msg: "Bienvenue chez CA"});
// });

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'nodeMongo';

// Create a new MongoClient
const client = new MongoClient(url);

// Use connect method to connect to the Server
client.connect(function (err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);

    //client.close();
});

http.createServer(app)
    .listen(
        app.get('port'),
        () => {
            console.log(`Express.js server ecoutes sur le port ${app.get('port')}`);
        });